class Article < ApplicationRecord
  belongs_to :user
  has_many :article_categories
  has_many :categories, through: :article_categories
  
  validates :user_id, presence: true
  validates :title, length: { minimum: 4, maximum: 120 }, presence: true
  validates :description, length: { minimum: 4, maximum: 65535}, presence: true
  
  def self.search(search)
    where("title LIKE ? OR description LIKE ?", "%#{search}%", "%#{search}%")
  end
  
end
