class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :set_search, only: [:search]
  before_action :require_user, except: [:index, :show, :search]
  before_action :require_same_user, only: [:destroy, :update, :edit]

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.paginate(page: params[:page], per_page: 5)
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
    
  end

  # POST /articles
  # POST /articles.json
  def create
     # render plain: params[:article].inspect  # debug.. see whats being passed in
     @article = Article.new(article_params)
     @article.user = current_user
     if @article.save
       flash[:success] = "Article was successfully created!"
       redirect_to article_path(@article)
     else
       flash[:danger] = "Error creating article!"
       render 'new'
     end
  end
  
  def search
    if params[:page] && session[:search]
      # searches each time which is stupid.. oh well..
      @articles = Article.search(session[:search]).order("created_at DESC").paginate(page: params[:page], per_page: 5)
    else
      if params[:search]
        @articles_search = Article.search(params[:search]).order("created_at DESC")
        if @articles_search
          @articles = @articles_search.paginate(page: params[:page], per_page: 5)
        else
          flash[:danger] = "Nothing found for "+params[:search]+"!"
          redirect_to articles_path
        end
      else
        flash[:danger] = "Nothing in search field!"
        redirect_to articles_path
      end
    end
  end
  

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    if @article.update(article_params)
      flash[:success] = 'Article was successfully updated.'
      redirect_to article_path(@article)
    else
      flash[:danger] = 'Unsuccessful edit!'
      render 'edit'
    
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    old_title = @article.title
    if @article.destroy
      flash[:success] = "Article '#{old_title}' was successfully deleted."
    else
      flash[:danger] = "Article '#{old_title}' could not be deleted!"
    end
    
    redirect_to articles_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    def set_search
      if params[:search] 
        session[:search] = params[:search]
      end
      if session[:search]
        @articles_search = Article.search(params[:search]).order("created_at DESC")
      else
        flash[:danger] = "No search param found!"
        redirect_to articles_path
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :description, category_ids: [])
    end

    def require_same_user
      if current_user != @article.user and !current_user.admin?
        flash[:danger] = "You can only edit or delete your own articles!"
        redirect_to root_path
      end
    end
    
end
